#!/bin/bash

LONGOPTS=debug,prefix:,offline,vpn,unwrap,mangohud,gamescope
OPTIONS=dp:onumg

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 1
fi
eval set -- "$PARSED"

dbg=0 prefix=- offline=0 vpn=0 unwrap=0 mangohud=0 gamescope=0

while true; do
    case "$1" in
        -d|--debug)
            dbg=1
            shift
            ;;
        -p|--prefix)
            prefix="$2"
            shift 2
            ;;
        -o|--offline)
            offline=1
            shift
            ;;
        -n|--vpn)
            vpn=1
            shift
            ;;
        -u|--unwrap)
            unwrap=1
            shift
            ;;
        -m|--mangohud)
            mangohud=1
            shift
            ;;
        -g|--gamescope)
            gamescope=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 2
            ;;
    esac
done

if [[ $# -eq 0 ]]; then
    echo "$0: Game to launch with wine is required."
    exit 3
fi

if [[ $offline -eq 1 ]] && [[ $vpn -eq 1 ]] ; then
    echo "$0: Can't use offline and vpn at the same time"
    exit 4
fi

export WINEPREFIX="${WINEPREFIX:-"${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"}"
export WINEARCH="${WINEARCH:=win64}"
export WINE_LARGE_ADDRESS_AWARE="${WINE_LARGE_ADDRESS_AWARE:=1}"
export WINEESYNC="${WINEESYNC:=1}"
export WINEFSYNC="${WINEFSYNC:=1}"
export DXVK_ENABLE_NVAPI="${DXVK_ENABLE_NVAPI:=1}"
export DXVK_ASYNC="${DXVK_ASYNC:=1}"

if [[ $dbg -eq 1 || $GAME_DEBUG -eq 1 ]]; then
    export WINEDEBUG="${WINEDEBUG:=}"
    export DXVK_LOG_LEVEL="${DXVK_LOG_LEVEL:=info}"
    export VKD3D_DEBUG="${VKD3D_DEBUG:=info}"
    export VKD3D_SHADER_DEBUG="${VKD3D_SHADER_DEBUG:=info}"
    export DXVK_NVAPI_LOG_LEVEL="${DXVK_NVAPI_LOG_LEVEL:=info}"
    export MANGOHUD_LOG_LEVEL="${MANGOHUD_LOG_LEVEL:=info}"
else
    export WINEDEBUG="${WINEDEBUG:=-all}"
    export DXVK_LOG_LEVEL="${DXVK_LOG_LEVEL:=error}"
    export VKD3D_DEBUG="${VKD3D_DEBUG:=err}"
    export VKD3D_SHADER_DEBUG="${VKD3D_SHADER_DEBUG:=err}"
    export MANGOHUD_LOG_LEVEL="${MANGOHUD_LOG_LEVEL:=err}"
fi

cmd=(nice -n -10 -- wine "$@")

if [[ $unwrap -ne 1 ]]; then
    if [[ $offline -ne 1 ]]; then
        net="--share-net"
    fi
    cmd=(bwrap
    --ro-bind /usr /usr
    --symlink usr/lib /lib
    --symlink usr/lib64 /lib64
    --symlink usr/bin /bin
    --symlink usr/sbin /sbin
    --ro-bind /etc /etc
    --tmpfs /tmp
    --ro-bind /tmp/.X11-unix/X0 /tmp/.X11-unix/X0
    # --bind /tmp/.wine-$UID /tmp/.wine-$UID
    --proc /proc
    --dev-bind /dev /dev
    --dir /var
    --dir /run
    --ro-bind /run/udev /run/udev
    --ro-bind /run/dbus /run/dbus
    --dir /run/user/$UID
    --ro-bind /run/user/$UID/pulse /run/user/$UID/pulse
    --ro-bind /run/user/$UID/bus /run/user/$UID/bus
    --ro-bind /sys /sys \
    --dir /home
    --ro-bind $HOME/.Xauthority $HOME/.Xauthority
    --bind "${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes" "${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes"
    --bind "${XDG_DATA_HOME:-$HOME/.local/share}/Goldberg SteamEmu Saves/settings" "${XDG_DATA_HOME:-$HOME/.local/share}/Goldberg SteamEmu Saves/settings"
    --bind $HOME/Games $HOME/Games
    --bind "${XDG_DATA_HOME:-$HOME/.local/share}/gamesdata" "${XDG_DATA_HOME:-$HOME/.local/share}/gamesdata"
    --unshare-all
    $net
    --
    "${cmd[@]}")
elif [[ $offline -eq 1 ]]; then
    cmd=(unshare -n -c -- "${cmd[@]}")
fi

if [[ $vpn -eq 1 ]]; then
    cmd=(netns-exec vpn "${cmd[@]}")
fi

if [[ $mangohud -eq 1 || $MANGOHUD -eq 1 ]]; then
    cmd=(mangohud "${cmd[@]}")
fi

if [[ $gamescope -eq 1 ]]; then
    cmd=(gamescope -f -- "${cmd[@]}")
fi

cd "$(dirname "$1")"

pre_launch () {
    xset s off -dpms
    systemctl --user start overclock
}

post_exit () {
    xset s on +dpms
    systemctl --user stop overclock
}

trap post_exit EXIT INT TERM
pre_launch

"${cmd[@]}"

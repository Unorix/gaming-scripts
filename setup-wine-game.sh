#!/usr/bin/bash

if [[ $# -ne 2 && $# -ne 3 ]]; then
    echo "Usage: $(basename $0) game_name game_folder [appid]"
    exit 1
fi

remove_drm() {
    shopt -s globstar nullglob
    cd "$1"

    for exe in **/*.exe; do
        echo "Running steamless on $exe"
        if steamless-cli "$exe" > /dev/null; then
            echo "Removed DRM on $exe"
            mv "${exe}" "${exe}.old"
            mv "${exe}.unpacked.exe" "${exe}"
        fi
    done
}

install_goldberg_emulator() {
    shopt -s globstar nullglob
    cd "$1"

    for dll in **/steam_api{,64}.dll; do
        [[ -f "${dll}.old" ]] && continue
        settings_dir="$(dirname "$dll")/steam_settings"
        if [[ ! -d "$settings_dir" ]]; then
            [[ ! -d "${2}_output" ]] && generate_emu_config "$2"
            cp -r "${2}_output/steam_settings" "$settings_dir"
            find_interfaces "$dll" > "${settings_dir}/steam_interfaces.txt"
        fi
        mv "${dll}" "${dll}.old"
        ln -s "/usr/share/goldberg-emulator/$(basename "$dll")" "$dll"
    done

    rm -rf "${2}_output" "backup" "login_temp"
}

select_exe() {
    shopt -s nullglob globstar
    cd "$1"

    PS3="Select .exe to start the game: "
    select exe in **/*.exe; do
        readlink -f "$exe"
        break
    done
}

extract_icon() {
    shopt -s nullglob lastpipe
    cd "$(dirname "$1")"

    game_slug=$(echo "$1" | iconv -t ascii//TRANSLIT | sed -E 's/[^a-zA-Z0-9]+/-/g' | sed -E 's/^-+|-+$//g' | tr A-Z a-z)
    exe_name=$(basename "$2" .exe)

    icoextract "$2" "${exe_name}.ico" || return $?
    convert "${exe_name}.ico" "${exe_name}.png"
    rm "${exe_name}.ico"

    for image in "${exe_name}"*.png; do
        identify -format '%w %h' "$image" | read w h

        if [[ $w -eq $h ]]; then
            xdg-icon-resource install --novendor --size $w "$image" "$game_slug"
        fi

        rm "$image"
    done
}

make_bin() {
    game_slug=$(echo "$1" | iconv -t ascii//TRANSLIT | sed -E 's/[^a-zA-Z0-9]+/-/g' | sed -E 's/^-+|-+$//g' | tr A-Z a-z)

    cat >"$HOME/bin/$game_slug" << END
#!/usr/bin/sh
run-wine-game -o -- "$2"
END
    chmod +x "$HOME/bin/$game_slug"
}

make_desktop_file() {
    game_slug=$(echo "$1" | iconv -t ascii//TRANSLIT | sed -E 's/[^a-zA-Z0-9]+/-/g' | sed -E 's/^-+|-+$//g' | tr A-Z a-z)

    folder="${XDG_DATA_HOME:-$HOME/.local/share}/applications/games"
    mkdir -p "$folder"
    cat >"$folder/${game_slug}.desktop" <<END
[Desktop Entry]
Name=$1
Exec=${game_slug}
Icon=${game_slug}
Terminal=false
Type=Application
Categories=Game;
END
}

remove_drm "$2"
[[ $3 ]] && install_goldberg_emulator "$2" "$3"
exe=$(select_exe "$2")
extract_icon "$1" "$exe"
make_bin "$1" "$exe"
make_desktop_file "$1" "$exe"


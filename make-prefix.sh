#!/bin/bash

export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export WINEARCH="${WINEARCH:=win64}"

wineboot -u

rm $WINEPREFIX/dosdevices/z:
winetricks -q isolate_home

winetricks -q corefonts cjkfonts
winetricks -q vcrun2019

winetricks -q win10
winetricks -q isolate_home
rm $WINEPREFIX/dosdevices/d::
ln -s $HOME/Games $WINEPREFIX/dosdevices/d:
mkdir -p "$WINEPREFIX/drive_c/users/$(whoami)/AppData/Roaming/Goldberg SteamEmu Saves"
ln -s "${XDG_DATA_HOME:-$HOME/.local/share}/Goldberg SteamEmu Saves/settings" "$WINEPREFIX/drive_c/users/$(whoami)/AppData/Roaming/Goldberg SteamEmu Saves/settings"

setup_dxvk install --symlink
setup_vkd3d_proton install --symlink
setup_dxvk_nvapi install --symlink
